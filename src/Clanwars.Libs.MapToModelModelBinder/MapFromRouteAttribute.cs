using System;

namespace Clanwars.Libs.MapToModelModelBinder;

[AttributeUsage(AttributeTargets.Property)]
public class MapFromRouteAttribute : Attribute
{
}
