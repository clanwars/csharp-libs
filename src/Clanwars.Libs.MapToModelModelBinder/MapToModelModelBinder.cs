using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Clanwars.Libs.MapToModelModelBinder;

public class MapToModelModelBinder : IModelBinder
{
    private readonly IModelBinder _bodyBinder;

    public MapToModelModelBinder(IModelBinder bodyBinder)
    {
        _bodyBinder = bodyBinder;
    }

    public async Task BindModelAsync(ModelBindingContext bindingContext)
    {
        if (bindingContext == null)
        {
            throw new ArgumentNullException(nameof(bindingContext));
        }

        await _bodyBinder.BindModelAsync(bindingContext);

        var modelTypeProperties = bindingContext.ModelType.GetProperties();
        var paramsToMap = modelTypeProperties
            .Where(pi => pi.GetCustomAttribute<MapFromRouteAttribute>() != null);
        var model = bindingContext.Result.Model;
        if (model == null) return;

        foreach (var paramToMap in paramsToMap)
        {
            var value = bindingContext.ValueProvider.GetValue(paramToMap.Name);

            ConvertType(value, model, paramToMap);
        }
    }

    private static void ConvertType(ValueProviderResult value, object? model, PropertyInfo paramToMap)
    {
        if (value.FirstValue is not null && paramToMap.PropertyType == typeof(Guid))
        {
            var v = Guid.Parse(value.FirstValue);
            paramToMap.SetValue(model, v);
        }
        else
        {
            paramToMap.SetValue(model, Convert.ChangeType(value.FirstValue, paramToMap.PropertyType));
        }
    }
}
