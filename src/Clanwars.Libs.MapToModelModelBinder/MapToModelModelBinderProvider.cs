using System;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;

namespace Clanwars.Libs.MapToModelModelBinder;

public class MapToModelModelBinderProvider : IModelBinderProvider
{
    private readonly BodyModelBinderProvider _bodyProvider;

    public MapToModelModelBinderProvider(BodyModelBinderProvider bodyProvider)
    {
        _bodyProvider = bodyProvider;
    }

    public IModelBinder? GetBinder(ModelBinderProviderContext context)
    {
        if (context == null)
        {
            throw new ArgumentNullException(nameof(context));
        }

        var modelTypeProperties = context.Metadata.ModelType.GetProperties();
        var anyPropertyHasMapFromRoute = modelTypeProperties.Any(p => p.GetCustomAttribute<MapFromRouteAttribute>() != null);

        return anyPropertyHasMapFromRoute ? new MapToModelModelBinder(_bodyProvider.GetBinder(context)!) : null;
    }
}
